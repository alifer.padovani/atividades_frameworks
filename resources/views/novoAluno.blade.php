<!DOCTYPE html>
<html>
<body>
<h2>Adicionar aluno</h2>
<form method="post" action="{{route('aluno.salvar')}}">
    @csrf

    <div>
        <label for="nome">Nome:</label>
        <input type="text" name="nome" required>
    </div>

    <div>
        <label for="data_nascimento">Data de nascimento:</label>
        <input type="date" name="data_nascimento" required>
    </div>

    <div>
        <label for="email">Email:</label>
        <input type="email" name="email" required>
    </div>

    <div>
        <label for="curso">Curso:</label>
        <input type="text" name="curso" required>
    </div>

    <div>
        <button type="submit">Salvar Aluno</button>
    </div>

</form>
</body>
</html>
