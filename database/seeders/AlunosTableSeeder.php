<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\support\facades\DB;

class AlunosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('alunos')->insert([
            ['nome' => 'alifer', 'email' => 'alifer@ifms.edu','data_nascimento' => '2000-01-01','curso' => 'Sistemas de Informação']
        ]

    );
    }
}
